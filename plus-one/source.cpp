class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        bool carry = false;
        int size = digits.size();
        int i = size -1;
        vector<int> num(size+1);
        vector<int>::reverse_iterator it = num.rbegin();
        for(;i >= 0; i--)
        {
            if(i == size-1)
            {
                *it = ++digits[i];
                cout << "1st " << *it<<"\n";
            }
            else
            {
                *it = digits[i] +(int) carry;
            }
            carry = *it > 9;
            *it %= 10;
            cout <<"test "<< *it;
            it++;
        }
        if(carry)
        {
            *(num.rend() -1) = (int) carry;
        }
        else {
            num.erase(num.begin());
        }
        return num;
    }
};
