class Solution {
public:
    int myAtoi(string s) {
        map<char, int> mymap;
        int minus =1;
        mymap['0'] = 0;
        mymap['1'] = 1;
        mymap['2'] = 2;
        mymap['3'] = 3;
        mymap['4'] = 4;
        mymap['5'] = 5;
        mymap['6'] = 6;
        mymap['7'] = 7;
        mymap['8'] = 8;
        mymap['9'] = 9;
        int result = 0, end =-1;
        for(int i = 0; i < s.size();i++)
        {
            if(s[i] == ' ')
            {
                continue;
            }
    
            if(s[i] <'0' || s[i] >'9')
            {
                if(end >= 0) {
                    break;
                }
                else if (end == -1) 
                {
                    if (s[i] == '-')
                    {
                        minus *= -1;
                        continue;
                    }
                    else if (s[i] == '+')
                    {
                        continue;
                    }           
                    return 0;
                }
            }
            else{
                if (result >= INT_MAX/10)
                {
                    return minus == -1 ? minus*INT_MAX - 1 : INT_MAX;
                }

                else
                    result *=10;
            }
            map<char, int>::iterator it = mymap.find(s[i]);
            if (it != mymap.end())
            {
                result += it->second;                
                end++;
            }

        }
        return minus*result;
        
    }
};
